package ru.vpavlova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.api.endpoint.IAdminUserEndpoint;
import ru.vpavlova.tm.api.service.dto.ISessionService;
import ru.vpavlova.tm.api.service.dto.IUserService;
import ru.vpavlova.tm.api.service.model.IUserGraphService;
import ru.vpavlova.tm.dto.Session;
import ru.vpavlova.tm.dto.User;
import ru.vpavlova.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    @Autowired
    private ISessionService sessionService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IUserGraphService userGraphService;

    @Override
    @WebMethod
    @SneakyThrows
    public void addUser(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "user", partName = "user") @NotNull User user
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        userService.add(user);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeUser(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "user", partName = "user") @NotNull User user
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        userService.remove(user);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeOneByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        userGraphService.removeByLogin(login);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void createUser(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @NotNull String login,
            @WebParam(name = "password", partName = "password") @NotNull String password
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        userGraphService.create(login, password);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void createUserWithEmail(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @NotNull String login,
            @WebParam(name = "password", partName = "password") @NotNull String password,
            @WebParam(name = "email", partName = "email") @NotNull String email
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        userGraphService.create(login, password, email);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void setUserPassword(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "userId", partName = "userId") @NotNull String userId,
            @WebParam(name = "password", partName = "password") @NotNull String password
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        userGraphService.setPassword(userId, password);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void updateUser(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "userId", partName = "userId") @NotNull String userId,
            @WebParam(name = "firstName", partName = "firstName") @Nullable String firstName,
            @WebParam(name = "lastName", partName = "lastName") @Nullable String lastName,
            @WebParam(name = "middleName", partName = "middleName") @Nullable String middleName
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        userGraphService.updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void lockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @NotNull String login
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        userService.lockUserByLogin(login);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "login", partName = "login") @NotNull String login
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        userService.unlockUserByLogin(login);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public List<User> findAllUsers(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        return userService.findAll();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void clearUsers(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        userService.clear();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void addAllUsers(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "users", partName = "users") @NotNull List<User> users
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        userService.addAll(users);
    }

}
