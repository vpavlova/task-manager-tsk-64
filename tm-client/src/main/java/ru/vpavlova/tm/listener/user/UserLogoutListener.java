package ru.vpavlova.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.event.ConsoleEvent;
import ru.vpavlova.tm.listener.AbstractUserListener;
import ru.vpavlova.tm.endpoint.Session;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;

@Component
public class UserLogoutListener extends AbstractUserListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Logout from program.";
    }

    @Override
    @EventListener(condition = "@userLogoutListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[LOGOUT]");
        if (sessionService == null) throw new ObjectNotFoundException();
        @Nullable final Session session = sessionService.getSession();
        sessionEndpoint.closeSession(session);
        sessionService.setSession(null);

    }

}
