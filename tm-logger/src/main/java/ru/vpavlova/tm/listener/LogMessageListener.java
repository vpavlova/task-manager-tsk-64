package ru.vpavlova.tm.listener;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.api.service.ILoggingService;
import ru.vpavlova.tm.service.LoggingService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

@Component
@AllArgsConstructor
public class LogMessageListener implements MessageListener {

    @NotNull
    @Autowired
    final ILoggingService loggingService = new LoggingService();

    @Override
    public void onMessage(Message message) {
        if (message instanceof ObjectMessage) {
            loggingService.writeLog(message);
        }
    }

}
