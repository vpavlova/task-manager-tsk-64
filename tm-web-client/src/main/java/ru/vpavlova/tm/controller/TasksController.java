package ru.vpavlova.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;
import ru.vpavlova.tm.model.Project;
import ru.vpavlova.tm.repository.ProjectRepository;
import ru.vpavlova.tm.repository.TaskRepository;

import java.util.List;

@Controller
public class TasksController {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping("/tasks")
    public ModelAndView index() {
        return new ModelAndView("task-list", "tasks", taskRepository.findAll());
    }

    @ModelAttribute("view_name")
    public String getViewName() {
        return "TASKS";
    }

    @ModelAttribute("projects")
    public List<Project> getProjects() {
        return projectRepository.findAll();
    }

}