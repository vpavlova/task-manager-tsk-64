package ru.vpavlova.tm.model;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import ru.vpavlova.tm.enumerated.Status;

import java.util.*;

@Getter
@Setter
public abstract class AbstractBusinessEntity {

    protected String id = UUID.randomUUID().toString();

    protected Date created = new Date();

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date dateFinish;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date dateStart;

    protected String description = "";

    protected String name = "";

    protected Status status = Status.NOT_STARTED;

    private String userId;

}
